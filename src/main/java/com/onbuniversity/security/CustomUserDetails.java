package com.onbuniversity.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.onbuniversity.domain.Role;
import com.onbuniversity.domain.User;

@SuppressWarnings("serial")
public class CustomUserDetails implements UserDetails{
	
	private String username;
	private String password;
	private User principal;
	
	Collection<? extends GrantedAuthority> authorities;
	
	public CustomUserDetails(User byUsername) {
		this.username = byUsername.getUsername();
		this.password = byUsername.getPassword();
		this.principal = byUsername;
		
		List<SimpleGrantedAuthority> userAuths = new ArrayList<SimpleGrantedAuthority>();

		for(Role role: byUsername.getRoles()) {
			userAuths.add(new SimpleGrantedAuthority(role.getRoleName()));
		}
		
		this.authorities = userAuths;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}
	
	public User getUser() {
		return principal;
	}
	
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	
}
