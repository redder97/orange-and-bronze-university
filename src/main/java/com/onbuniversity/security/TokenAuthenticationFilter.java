package com.onbuniversity.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.onbuniversity.util.RequestUtil;


public class TokenAuthenticationFilter extends OncePerRequestFilter {
	
private static final Logger logger = LoggerFactory.getLogger(TokenAuthenticationFilter.class);
	
	@Autowired
	TokenProvider tokenProvider;
	
	@Autowired
	UserDetailsService userDetailsService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		try {
			String jwt = RequestUtil.getJwtFromRequest(request);
			
			if(StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
				String username = tokenProvider.getUsernameFromToken(jwt);
				
				UserDetails userDetails = userDetailsService.loadUserByUsername(username);
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				
				logger.info("Username from Token: " + username);
				logger.info("Roles from Token: " + tokenProvider.getAuthorities(jwt));;
				
				SecurityContextHolder.getContext().setAuthentication(authentication);
				
				
			}
		}catch(Exception e) {
			logger.error("Could not set authentication in Security Context.", e);
		}
		
        filterChain.doFilter(request, response);
		
	}
	
	
	
	

}
