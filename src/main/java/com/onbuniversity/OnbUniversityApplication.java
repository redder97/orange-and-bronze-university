package com.onbuniversity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.onbuniversity.security.SecurityConfig;

@Import({ SecurityConfig.class})
@SpringBootApplication
public class OnbUniversityApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnbUniversityApplication.class, args);
	}

}
