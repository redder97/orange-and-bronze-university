package com.onbuniversity.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
/*	
 * As much as possible immutable
 * As much as possible no getters & setters,
 * If must, build through constructor
 * business logic methods is OK.
 * 
 * Defensive Copy (return Object with different memory reference.)
 * 
 * Always have protected no-args constructor 
 * 
 * implement equals and hashCode, toString()[id will do, Never put collection obj] 
 * 
 * */
@Entity
public class Section {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String sectionName;
	private int capacity;
	
	@Enumerated(EnumType.STRING)
	private Term term;
	
	@OneToOne
	private Course course;
	
	@OneToMany
	@JoinColumn(name="fk_day")
	private List<SectionDetail> days;
	
	// ManyToMany - make set
	@ManyToMany
	private List<Student> students;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Term getTerm() {
		return term;
	}

	public void setTerm(Term term) {
		this.term = term;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public List<SectionDetail> getDays() {
		return days;
	}

	public void setDays(List<SectionDetail> days) {
		this.days = days;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}
	
	public void addStudent(Student student) {
		students.add(student);
		capacity--;
	}
	
	public boolean removeStudent(Student student) {
		if(students.removeIf(s -> s.getId() == student.getId())) {
			capacity++;
			return true;
		}	
		return false;
	}
	

}


