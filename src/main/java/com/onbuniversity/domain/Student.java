package com.onbuniversity.domain;

import java.time.LocalTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.onbuniversity.exception.ConflictingScheduleException;
import com.onbuniversity.exception.NoSlotsException;
import com.onbuniversity.exception.StudentAlreadyEnlistedException;
import com.onbuniversity.exception.StudentCompletedCourseException;

@Entity
public class Student extends User {

	@OneToMany
	private List<CompletedCourse> completedCourses;
	
	@ManyToMany
	@JsonBackReference
	private List<Section> sections;
	
	@ManyToOne
	@JoinColumn(name="fk_degree")
	@JsonManagedReference
	private Degree degree;
	
	
	public List<CompletedCourse> getCompletedCourses() {
		return completedCourses;
	}

	public void setCompletedCourses(List<CompletedCourse> completedCourses) {
		this.completedCourses = completedCourses;
	}

	public List<Section> getSections() {
		return sections;
	}

	public void setSections(List<Section> sections) {
		this.sections = sections;
	}

	public Degree getDegree() {
		return degree;
	}

	public void setDegree(Degree degree) {
		this.degree = degree;
	}
	
	
	public boolean enlistInSection(Section section){
		for(Section s : sections) {
			if(s == section) {
				throw new StudentAlreadyEnlistedException("Student already enrolled in section.");
			}
		}
		
		for(CompletedCourse c : completedCourses) {
			if(section.getCourse().getCourseName().equalsIgnoreCase(c.getCourse().getCourseName())) {
				throw new StudentCompletedCourseException("Student has already completed the course.");
			}
		}
		
	
		for(Section s : sections) {
			for(int i = 0 ; i < section.getDays().size() ; i++) {
				if(isOverlapping(section.getDays().get(i), s.getDays().get(i))) {
					throw new ConflictingScheduleException("There is a conflicting schedule: " + s.getSectionName());
				}
			}
		}
		
		if(section.getCapacity() == 0) {
			throw new NoSlotsException("No slots in section: " + section.getSectionName());
		}
		
		sections.add(section);
		return true;
	}
	
	private boolean isOverlapping(SectionDetail s1, SectionDetail s2) {
		
		LocalTime time1Start = s1.getStartTime();
		LocalTime time1End = s1.getEndTime();
		
		LocalTime time2Start = s2.getStartTime();
		LocalTime time2End = s2.getEndTime();
		
		return !(time1End.isBefore(time2Start) || time1End.equals(time2Start)) 
				&& !(time1Start.isAfter(time2End) || time1Start.equals(time2End));
		
	}
	
	public boolean disenrollFromSection(Section section) throws Exception {
		
		if(sections.removeIf(sec -> sec.getId() == section.getId())) {
			return section.removeStudent(this);
		}
		return false;
		
	}
	
	
	
}
