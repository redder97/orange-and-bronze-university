package com.onbuniversity.payload;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JwtAuthenticationResponse {

	private String accessToken;
    private String tokenType = "Bearer";
    private Collection<? extends GrantedAuthority> auths; 
    
    public JwtAuthenticationResponse(String accessToken, Collection<? extends GrantedAuthority> auths) {
        this.accessToken = accessToken;
        this.auths = auths; 
     
    }
    
    public Collection<? extends GrantedAuthority> getAuths() {
		return auths;
	}

	public void setAuths(Collection<? extends GrantedAuthority> auths) {
		this.auths = auths;
	}
	
	public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

}
