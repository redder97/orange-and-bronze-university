package com.onbuniversity.payload;

public class ErrorResponse{
	
	private int status;
	private String message;
	
	public ErrorResponse(String message, int status) {
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}
	
	

}
