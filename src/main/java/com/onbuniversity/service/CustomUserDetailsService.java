package com.onbuniversity.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.onbuniversity.domain.User;
import com.onbuniversity.security.CustomUserDetails;

@Service
public class CustomUserDetailsService implements UserDetailsService{
	
	private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);	
	
	@Autowired
	StudentService studentService;
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = studentService.findByUsername(username);
		CustomUserDetails userDetails = new CustomUserDetails(user);
		logger.info("User: " + user.getName());
		logger.info("userDetails: " + userDetails.getAuthorities());
		return userDetails;
		
	}

}
