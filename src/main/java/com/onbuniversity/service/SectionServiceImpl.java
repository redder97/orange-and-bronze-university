package com.onbuniversity.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onbuniversity.converter.SectionConverter;
import com.onbuniversity.domain.Section;
import com.onbuniversity.domain.Student;
import com.onbuniversity.dto.SectionDTO;
import com.onbuniversity.repository.SectionRepository;
import com.onbuniversity.repository.StudentRepository;

@Service
@Transactional
public class SectionServiceImpl implements SectionService{
	
	@Autowired
	SectionRepository sectionRepository;
	
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	SectionConverter sectionConverter;
	
	@Transactional(readOnly=true)
	@Override
	public List<SectionDTO> getAllSections() {
		List<SectionDTO> sections = new ArrayList<>();
		
		for(Section s : sectionRepository.findAll()) {
			sections.add(sectionConverter.createDTO(s));
		}
		
		return sections;
		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Section> findByStudent(Student student) {
		return sectionRepository.findByStudents(student);
	}

	@Override
	public SectionDTO addStudentToSection(Long studentId, Long sectionId){
		
		Student student = studentRepository.findById(studentId).get();
		Section section = sectionRepository.findById(sectionId).get();
		
		
		if(student.enlistInSection(section)) {
			section.addStudent(student);
			studentRepository.save(student);
			sectionRepository.save(section);
				
			return sectionConverter.createDTO(section);
		}
		
		
		return null; 
		
	}
	/*
	 * For all layers: Validate
	 * Always validate <<Validate, logic, return>>
	 * 
	 * As much as possible Specific Exception
	 * 
	 * Read up on Optional<?>
	 * 
	 * */
	@Override
	public SectionDTO removeStudentFromSection(Long studentId, Long sectionId) throws Exception {
		
		Student student = studentRepository.findById(studentId).get();
		Section section = sectionRepository.findById(sectionId).get();
		
		try {
			if(student.disenrollFromSection(section)) {
				studentRepository.save(student);
				sectionRepository.save(section);
				
				return sectionConverter.createDTO(section);
			}	
		}catch(Exception e) {
			throw new Exception("Student is not enrolled in section!");
		}
		//Never Return null (either no value object) 
		return null;
		
	}


	@Override
	public List<Section> removeSectionById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Section saveSection(Section section) {
		return sectionRepository.save(section);
	}


	@Override
	@Transactional(readOnly=true)
	public SectionDTO findSectionById(Long id) {
		return sectionConverter.createDTO(sectionRepository.findById(id).get());
	}
	
//	public List<SectionDTO> removeSection(Long sectionId){
//		Section section = sectionService.findById(id);
//		List<Student> allStudents = studentService.getAllStudents();
//		
//		for(Student s : allStudents) {
//			List<Section> sectionsOfStudent = sectionService.findByStudent(s);
//			sectionsOfStudent.removeIf(sec -> sec.getId() == id);
//			
//			s.setSections(sectionsOfStudent);
//			studentService.saveStudent(s);
//		}
//		
//		Course course = courseService.findById(section.getCourse().getId());
//		course.setSection(null);
//		courseService.saveCourse(course);
//		
//		sectionService.removeSectionById(id);
//		
//		return sectionService.getAllSections();
//	}
//	
	
	

}
