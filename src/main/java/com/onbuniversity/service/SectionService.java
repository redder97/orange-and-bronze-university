package com.onbuniversity.service;

import java.util.List;

import com.onbuniversity.domain.Section;
import com.onbuniversity.domain.Student;
import com.onbuniversity.dto.SectionDTO;

public interface SectionService {
	
	SectionDTO findSectionById(Long id);
	Section saveSection(Section section);
	List<SectionDTO> getAllSections();
	List<Section> findByStudent(Student student);
	List<Section> removeSectionById(Long id);
	SectionDTO addStudentToSection(Long studentId, Long sectionId);
	SectionDTO removeStudentFromSection(Long studentId, Long sectionId) throws Exception;
}
