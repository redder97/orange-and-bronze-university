package com.onbuniversity.service;

import com.onbuniversity.domain.Role;

public interface RoleService {
	
	Role saveRole(Role role);
}
