package com.onbuniversity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onbuniversity.domain.Course;
import com.onbuniversity.repository.CourseRepository;

@Service
@Transactional
public class CourseServiceImpl implements CourseService {
	
	@Autowired
	CourseRepository courseRepository;
	
	@Transactional(readOnly=true)
	public List<Course> getAllCourses() {
		return courseRepository.findAll();
	}
	
	public Course saveCourse(Course course) {
		return courseRepository.save(course);
	}

	@Override
	@Transactional(readOnly=true)
	public Course findById(Long id) {
		return courseRepository.findById(id).get();
	}
	
	
	
}