package com.onbuniversity.service;

import java.util.List;

import com.onbuniversity.domain.Degree;
import com.onbuniversity.domain.Student;
import com.onbuniversity.dto.DegreeDTO;
import com.onbuniversity.dto.StudentDTO;

public interface DegreeService {
	
	List<DegreeDTO> getAll();
	Degree saveDegree(Degree degree);
	DegreeDTO findById(Long id);
	Degree findByDegreeName(String degreeName);
	Degree findByStudent(Student student);
	
	StudentDTO enlistStudent(Long degreeId, Long studentId);
	List<StudentDTO> getStudentsByDegree(Long degreeId);
	StudentDTO removeStudentFromDegree(Long degreeId, Long studentId);

}
