package com.onbuniversity.service;

import java.util.List;

import com.onbuniversity.domain.Degree;
import com.onbuniversity.domain.Student;
import com.onbuniversity.dto.NewStudentDTO;
import com.onbuniversity.dto.StudentDTO;

public interface StudentService{
	
	Student saveStudent(Student student);
	Student findByName(String name);
	List<StudentDTO> getAllStudents();
	Student addStudentToSection(Student student);
	Student findByUsername(String username);
	List<Student> findByDegree(Degree degree);
	List<Student> deleteStudentById(Long id);
	Student findById(Long id);
	
	StudentDTO getStudentProfile(String username);
	StudentDTO createStudentProfile(NewStudentDTO student);
	StudentDTO updateStudent(StudentDTO student, Long studentId);
}
