package com.onbuniversity.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onbuniversity.converter.DegreeConverter;
import com.onbuniversity.converter.StudentConverter;
import com.onbuniversity.domain.Degree;
import com.onbuniversity.domain.Student;
import com.onbuniversity.dto.DegreeDTO;
import com.onbuniversity.dto.StudentDTO;
import com.onbuniversity.exception.IllegalDegreeReferenceException;
import com.onbuniversity.exception.StudentAlreadyEnlistedException;
import com.onbuniversity.repository.DegreeRepository;
import com.onbuniversity.repository.StudentRepository;

/*
 * Always annotate with @Transaction (if class level = read only false)
 * 
 * */
@Service
@Transactional
public class DegreeServiceImpl implements DegreeService{
	
	@Autowired
	DegreeRepository degreeRepository;
	
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	DegreeConverter degreeConverter;
	
	@Autowired
	StudentConverter studentConverter;
	
	@Override
	public Degree saveDegree(Degree degree) {
		return degreeRepository.save(degree);
	}
	
	@Override
	@Transactional(readOnly=true)
	public DegreeDTO findById(Long id) {
		Degree e = degreeRepository.findById(id).get();
		return degreeConverter.createDTO(e);
	}
	
	@Override
	@Transactional(readOnly=true)
	public Degree findByDegreeName(String degreeName) {
		return degreeRepository.findByDegreeName(degreeName);
	}
	
	@Override
	public List<DegreeDTO> getAll() {
		List<Degree> degrees = degreeRepository.findAll();
		List<DegreeDTO> degreeDTOs = new ArrayList<>();
		
		for(Degree d : degrees) {	
			degreeDTOs.add(degreeConverter.createDTO(d));
		}
		
		return degreeDTOs;
	}

	@Override
	@Transactional(readOnly=true)
	public Degree findByStudent(Student student) {
		return degreeRepository.findByStudents(student);
	}

	@Override
	public StudentDTO enlistStudent(Long degreeId, Long studentId) {
		Student student = studentRepository.findById(studentId).get();
		Degree degree = degreeRepository.findById(degreeId).get();
		
		if(student.getDegree() != null) {
			if(student.getDegree().getDegreeName().equals(degree.getDegreeName()))
				throw new StudentAlreadyEnlistedException("Student is already registered in degree.");
			else
				throw new StudentAlreadyEnlistedException("Student is currently taking up a degree: " + degree.getDegreeName());
		}else {
			student.setDegree(degree);
			degree.getStudents().add(student);
			
			studentRepository.save(student);
			degreeRepository.save(degree);
		}
		
		return studentConverter.createDTO(studentRepository.findById(studentId).get());
	}

	@Override
	@Transactional(readOnly=true)
	public List<StudentDTO> getStudentsByDegree(Long degreeId) {
		Degree degree = degreeRepository.findById(degreeId).get();
		List<Student> students = studentRepository.findByDegree(degree);
		
		List<StudentDTO> studentDTOs = new ArrayList<>();
		for(Student s : students) {
			studentDTOs.add(studentConverter.createDTO(s));
		}
		
		return studentDTOs;
		
	}

	@Override
	public StudentDTO removeStudentFromDegree(Long degreeId, Long studentId){
		Student student = studentRepository.findById(studentId).get();
		Degree degree = degreeRepository.findById(degreeId).get();
		
		if(student.getDegree() == null) {
			throw new IllegalDegreeReferenceException("Student is not taking a degree!");
			
		}else if(student.getDegree().getId() == degreeId) {
			
			student.setDegree(null);
			degree.getStudents().removeIf(s -> s.getId() == studentId);
			
			studentRepository.save(student);
			degreeRepository.save(degree);
		}else {
			throw new IllegalDegreeReferenceException("Student is not taking the degree!");
		}
		return studentConverter.createDTO(studentRepository.findById(studentId).get());
	}
	
	
	
	
	

}
