package com.onbuniversity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onbuniversity.domain.SectionDetail;
import com.onbuniversity.repository.SectionDetailRepository;

@Service
@Transactional
public class SectionDetailImpl implements SectionDetailService {
	
	@Autowired
	SectionDetailRepository sectionDetailRepository;
	
	@Transactional(readOnly=true)
	public List<SectionDetail> getAllSectionDetails() {
		return sectionDetailRepository.findAll();
	}

	public SectionDetail saveSectionDetail(SectionDetail sectionDetail) {
		return sectionDetailRepository.save(sectionDetail);
	}


}
