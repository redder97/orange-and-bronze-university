package com.onbuniversity.service;

import java.util.List;

import com.onbuniversity.domain.SectionDetail;

public interface SectionDetailService {
	
	List<SectionDetail> getAllSectionDetails();
	SectionDetail saveSectionDetail(SectionDetail sectionDetail);
	

}
