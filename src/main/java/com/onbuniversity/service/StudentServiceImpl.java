package com.onbuniversity.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onbuniversity.converter.StudentConverter;
import com.onbuniversity.domain.Degree;
import com.onbuniversity.domain.Role;
import com.onbuniversity.domain.Student;
import com.onbuniversity.dto.NewStudentDTO;
import com.onbuniversity.dto.StudentDTO;
import com.onbuniversity.repository.RoleRepository;
import com.onbuniversity.repository.StudentRepository;

@Service
@Transactional
public class StudentServiceImpl implements StudentService{
	
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	StudentConverter studentConverter;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Override
	@Transactional(readOnly=true)
	public List<StudentDTO> getAllStudents(){
		List<Student> students = studentRepository.findAll();
		List<StudentDTO> studentDTOs = new ArrayList<>();
		
		for(Student s : students) {
			studentDTOs.add(studentConverter.createDTO(s));
		}
		
		return studentDTOs;
	}
	
	public Student addStudentToSection(Student student) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Transactional(readOnly=true)
	public Student findByName(String name) {
		return studentRepository.findByName(name);
	}
	
	@Override
	@Transactional(readOnly=true)
	public Student findByUsername(String username) {
		return studentRepository.findByUsername(username);
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<Student> findByDegree(Degree degree) {
		return studentRepository.findByDegree(degree);
	}


	@Override
	@Transactional(readOnly=true)
	public StudentDTO getStudentProfile(String username) {
		Student e = studentRepository.findByUsername(username);
		return studentConverter.createDTO(e);
		
	}

	@Override
	public List<Student> deleteStudentById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Student saveStudent(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public Student findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StudentDTO createStudentProfile(NewStudentDTO student) {
		
		Student e = studentConverter.createNewStudent(student);
		
		Role studentRole = roleRepository.findByRoleName("ROLE_STUDENT");
		List<Role> studentRoleList = new ArrayList<>();
		studentRoleList.add(studentRole);
		e.setRoles(studentRoleList);
		
		studentRepository.save(e);
		
		return studentConverter.createDTO(studentRepository.findByUsername(e.getUsername()));
	}

	@Override
	public StudentDTO updateStudent(StudentDTO student, Long studentId) {
		
		Student e = studentRepository.findById(studentId).get();
		Student updatedStudentEntity = studentConverter.updateStudentToEntity(student, e);

		studentRepository.save(updatedStudentEntity);
		
		return studentConverter.createDTO(studentRepository.findById(updatedStudentEntity.getId()).get());
		
	}

	

	


	
	
}
