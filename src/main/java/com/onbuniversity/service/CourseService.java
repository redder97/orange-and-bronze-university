package com.onbuniversity.service;

import java.util.List;

import com.onbuniversity.domain.Course;

public interface CourseService {
	
	List<Course> getAllCourses();
	Course saveCourse(Course course);
	Course findById(Long id);
	
}
