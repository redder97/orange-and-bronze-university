package com.onbuniversity.converter;

public interface Converter<ObjectDTO, Entity> {
	
	ObjectDTO createDTO(Entity e);
	Entity toEntity(ObjectDTO o);
	
	
}
