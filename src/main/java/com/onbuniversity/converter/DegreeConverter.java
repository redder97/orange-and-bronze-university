package com.onbuniversity.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.onbuniversity.domain.Course;
import com.onbuniversity.domain.Degree;
import com.onbuniversity.domain.Student;
import com.onbuniversity.dto.DegreeDTO;
import com.onbuniversity.dto.StudentDTO;
import com.onbuniversity.service.DegreeService;

@Component
public class DegreeConverter implements Converter<DegreeDTO, Degree>{
	
	@Autowired
	DegreeService degreeService;
	
	@Override
	public DegreeDTO createDTO(Degree e) {
		
		List<String> courseList = new ArrayList<>();
		List<StudentDTO> studentsInDegree = new ArrayList<>();
		
		for(Course c : e.getRequiredCourses()) {
			courseList.add(c.getCourseName());
		}
		
		DegreeDTO dto = new DegreeDTO(
				e.getDegreeName(), 
				courseList);
		dto.setId(e.getId());
		
		try {
			if(degreeService.getStudentsByDegree(dto.getId()).size() > 0) {
				studentsInDegree = degreeService.getStudentsByDegree(dto.getId());
				dto.setStudentsInDegree(studentsInDegree);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
		return dto;
	}

	@Override
	public Degree toEntity(DegreeDTO o) {
		// TODO Auto-generated method stub
		return null;
	}

}
