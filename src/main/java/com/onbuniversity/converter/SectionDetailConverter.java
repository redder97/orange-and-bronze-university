
package com.onbuniversity.converter;

import org.springframework.stereotype.Component;

import com.onbuniversity.domain.SectionDetail;
import com.onbuniversity.dto.SectionDetailDTO;

@Component
public class SectionDetailConverter implements Converter<SectionDetailDTO, SectionDetail>{

	@Override
	public SectionDetailDTO createDTO(SectionDetail e) {
		SectionDetailDTO o = new SectionDetailDTO();
		o.setDay(e.getDay());
		o.setStartTime(e.getStartTime());
		o.setEndTime(e.getEndTime());
		o.setId(e.getId());
		o.setRoom(e.getRoom());
		o.setDay(e.getDay());
		
		return o;
	}

	@Override
	public SectionDetail toEntity(SectionDetailDTO o) {
		// TODO Auto-generated method stub
		return null;
	}

}
