package com.onbuniversity.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.onbuniversity.domain.Section;
import com.onbuniversity.domain.SectionDetail;
import com.onbuniversity.domain.Student;
import com.onbuniversity.dto.SectionDTO;
import com.onbuniversity.dto.SectionDetailDTO;
import com.onbuniversity.dto.StudentDTO;
/*
 * DTOs must be abstracted away from Entity
 * 
 * */
@Component
public class SectionConverter implements Converter<SectionDTO, Section> {
	
	@Autowired
	SectionDetailConverter sectionDetailConverter;
	
	@Override
	public SectionDTO createDTO(Section e) {
		
		SectionDTO sectionDTO = new SectionDTO();
		List<StudentDTO> students = new ArrayList<>();
		List<SectionDetailDTO> days = new ArrayList<>();
		Map<Long, String> course = new HashMap<>();
		
		
		course.put(e.getCourse().getId(), e.getCourse().getCourseName());
		
		sectionDTO.setId(e.getId());
		sectionDTO.setCapacity(e.getCapacity());
		sectionDTO.setCourse(course);
		
		
		for(Student s : e.getStudents()) {
			StudentDTO student = new StudentDTO();
			student.setId(s.getId());
			student.setName(s.getName());
			students.add(student);
		}
		
		sectionDTO.setStudents(students);
		
		sectionDTO.setSectionName(e.getSectionName());
		sectionDTO.setTerm(e.getTerm());
		
		for(SectionDetail s : e.getDays()) {
			days.add(sectionDetailConverter.createDTO(s));
		}
		
		sectionDTO.setDays(days);
		
		return sectionDTO;
		
		
	}

	@Override
	public Section toEntity(SectionDTO o) {
		// TODO Auto-generated method stub
		return null;
	}

}
