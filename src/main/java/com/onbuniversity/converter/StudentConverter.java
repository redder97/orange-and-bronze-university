package com.onbuniversity.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.onbuniversity.domain.Student;
import com.onbuniversity.dto.NewStudentDTO;
import com.onbuniversity.dto.StudentDTO;

@Component
public class StudentConverter implements Converter<StudentDTO, Student> {
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Override
	public StudentDTO createDTO(Student e) {
		
		StudentDTO studentDTO = new StudentDTO();
		studentDTO.setContactNumber(e.getContactNumber());
		
		if(e.getDegree() != null) {
			studentDTO.setDegree(e.getDegree().getDegreeName());
		}
		
		studentDTO.setEmailAddress(e.getEmailAddress());
		studentDTO.setId(e.getId());
		studentDTO.setName(e.getName());
		
		return studentDTO;
		
	}
	

	@Override
	public Student toEntity(StudentDTO o) {
		return null;
		
	}
	
	
	public Student createNewStudent(NewStudentDTO o) {
		
		Student student = new Student();
		
		student.setName(o.getName());
		student.setUsername(o.getUsername());
		student.setPassword(passwordEncoder.encode(o.getPassword()));
		student.setContactNumber(o.getContactNumber());
		student.setEmailAddress(o.getEmailAddress());
		
		return student;
	}
	
	public Student updateStudentToEntity(StudentDTO o, Student e) {
	
		e.setContactNumber(o.getContactNumber());
		e.setEmailAddress(o.getEmailAddress());
		
		return e;
		
	}


	

}
