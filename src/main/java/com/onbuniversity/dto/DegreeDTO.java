package com.onbuniversity.dto;

import java.io.Serializable;
import java.util.List;

public class DegreeDTO implements Serializable {
	
	private Long id;
	private String degreeName;
	private List<String> requiredCourses;
	private List<StudentDTO> studentsInDegree;
	
	public DegreeDTO(String degreeName, List<String> requiredCourses) {
		this.degreeName = degreeName;
		this.requiredCourses = requiredCourses;
	}
	
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDegreeName() {
		return degreeName;
	}

	public void setDegreeName(String degreeName) {
		this.degreeName = degreeName;
	}

	public List<String> getRequiredCourses() {
		return requiredCourses;
	}

	public void setRequiredCourses(List<String> requiredCourses) {
		this.requiredCourses = requiredCourses;
	}



	public List<StudentDTO> getStudentsInDegree() {
		return studentsInDegree;
	}



	public void setStudentsInDegree(List<StudentDTO> studentsInDegree) {
		this.studentsInDegree = studentsInDegree;
	}
	
	
	
	
	
	

}
