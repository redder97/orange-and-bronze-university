package com.onbuniversity.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.onbuniversity.domain.Term;

public class SectionDTO implements Serializable {
	
	private Long id;
	
	private String sectionName;
	private int capacity;
	
	private Term term;
	
	private Map<Long, String> course;
	private List<SectionDetailDTO> days;
	private List<StudentDTO> students;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Term getTerm() {
		return term;
	}

	public void setTerm(Term term) {
		this.term = term;
	}

	public Map<Long, String> getCourse() {
		return course;
	}

	public void setCourse(Map<Long, String> course) {
		this.course = course;
	}

	public List<SectionDetailDTO> getDays() {
		return days;
	}

	public void setDays(List<SectionDetailDTO> days) {
		this.days = days;
	}

	public List<StudentDTO> getStudents() {
		return students;
	}

	public void setStudents(List<StudentDTO> students) {
		this.students = students;
	}
	

}
