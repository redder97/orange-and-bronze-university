package com.onbuniversity.dto;


import java.io.Serializable;

import com.onbuniversity.domain.Section;

public class CourseDTO implements Serializable {
	

	private Long id;
	private String courseName;

	private Section section;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}
	

}
