package com.onbuniversity.dto;

import java.io.Serializable;

/*
 * Make sure all DTOs implements Serializable <for java compiler,
 * necessary when changing format of data>
 * 
 * Getters will suffice, 
 * 
 * If possible, always populate via constructor
 * 
 * Builder Pattern, Static Factory Pattern
 * 
 * As much as possible; annotations validators
 * 
 * */
public class StudentDTO implements Serializable{
	
	private Long id;
	private String name;
	private String degree;
	private String contactNumber;
	private String emailAddress;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	
	

}
