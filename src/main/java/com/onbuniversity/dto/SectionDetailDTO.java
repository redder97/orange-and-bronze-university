package com.onbuniversity.dto;

import java.io.Serializable;
import java.time.LocalTime;

import com.onbuniversity.domain.Day;
import com.onbuniversity.domain.Room;


public class SectionDetailDTO implements Serializable {
	
	private Long id;
	private Day day;
	
	private LocalTime startTime;
	private LocalTime endTime;

	private Room room;

	public Day getDay() {
		return day;
	}
	public void setDay(Day day) {
		this.day = day;
	}
	public LocalTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}
	public LocalTime getEndTime() {
		return endTime;
	}
	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Room getRoom() {
		return room;
	}
	public void setRoom(Room room) {
		this.room = room;
	}

	
	
}
