package com.onbuniversity;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.onbuniversity.domain.Course;
import com.onbuniversity.domain.Day;
import com.onbuniversity.domain.Degree;
import com.onbuniversity.domain.Role;
import com.onbuniversity.domain.Room;
import com.onbuniversity.domain.Section;
import com.onbuniversity.domain.SectionDetail;
import com.onbuniversity.domain.Student;
import com.onbuniversity.domain.Term;
import com.onbuniversity.service.CourseService;
import com.onbuniversity.service.DegreeService;
import com.onbuniversity.service.RoleService;
import com.onbuniversity.service.SectionDetailService;
import com.onbuniversity.service.SectionService;
import com.onbuniversity.service.StudentService;

@Component
public class BootStrapper implements CommandLineRunner{
	
	@Autowired
	StudentService studentService;
	
	@Autowired
	DegreeService degreeService;
	
	@Autowired
	CourseService courseService;
	
	@Autowired
	SectionService sectionService;

	@Autowired
	SectionDetailService sectionDetailService;	
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	public void run(String... args) throws Exception {
		
		//Roles
		Role adminRole = new Role();
		adminRole.setRoleName("ADMIN");
		roleService.saveRole(adminRole);
		
		Role studentRole = new Role();
		studentRole.setRoleName("STUDENT");
		roleService.saveRole(studentRole);
		
		List<Role> rolesForAdmin = new ArrayList<Role>();
		rolesForAdmin.add(adminRole);
		
		//ADMIN
		Student admin = new Student();
		admin.setUsername("admin");
		admin.setName("redam");
		admin.setContactNumber("12458795");
		admin.setEmailAddress("rere@gamil.com");
		admin.setPassword(passwordEncoder.encode("adminPass"));
		admin.setRoles(rolesForAdmin);
		
		
		studentService.saveStudent(admin);
		
		
		
		
		//Create Courses
		Course c1 = new Course(), c2 = new Course(), c3 = new Course();
		c1.setCourseName("Intro to Java");
		c2.setCourseName("Intro to Test Driven Development");
		c3.setCourseName("Intro to PHP");
		
		//Save Courses
		courseService.saveCourse(c1);
		courseService.saveCourse(c2);
		courseService.saveCourse(c3);
		
		//Create List of Courses
		List<Course> courses = new ArrayList<Course>();
		courses.add(c1);
		courses.add(c2);
		courses.add(c3);
		
		//Create SectionDetail of c1
		SectionDetail sd1 = new SectionDetail();
		sd1.setStartTime(LocalTime.of(1, 0));
		sd1.setEndTime(LocalTime.of(2, 00));
		sd1.setRoom(Room.ROOM1);
		sd1.setDay(Day.MONDAY);
		sectionDetailService.saveSectionDetail(sd1);
		
		SectionDetail sd2 = new SectionDetail();
		sd2.setStartTime(LocalTime.of(2,00));
		sd2.setEndTime(LocalTime.of(3,00));
		sd2.setRoom(Room.ROOM2);
		sd2.setDay(Day.WED);
		sectionDetailService.saveSectionDetail(sd2);
		
		SectionDetail sd3 = new SectionDetail();
		sd3.setStartTime(LocalTime.of(1,00));
		sd3.setEndTime(LocalTime.of(5,00));
		sd3.setRoom(Room.ROOM3);
		sd3.setDay(Day.FRIDAY);
		sectionDetailService.saveSectionDetail(sd3);
		
		List<SectionDetail> schedC1 = new ArrayList<>();
		schedC1.add(sd1);
		schedC1.add(sd2);
		schedC1.add(sd3);
		
		//Create SectionDetail of c2
		SectionDetail sd4 = new SectionDetail();
		sd4.setStartTime(LocalTime.of(1, 0));
		sd4.setEndTime(LocalTime.of(2, 00));
		sd4.setRoom(Room.ROOM2);
		sd4.setDay(Day.MONDAY);
		sectionDetailService.saveSectionDetail(sd4);
		
		SectionDetail sd5 = new SectionDetail();
		sd5.setStartTime(LocalTime.of(2,00));
		sd5.setEndTime(LocalTime.of(3,00));
		sd5.setRoom(Room.ROOM3);
		sd5.setDay(Day.WED);
		sectionDetailService.saveSectionDetail(sd5);
		
		SectionDetail sd6 = new SectionDetail();
		sd6.setStartTime(LocalTime.of(4,00));
		sd6.setEndTime(LocalTime.of(5,00));
		sd6.setRoom(Room.ROOM3);
		sd6.setDay(Day.FRIDAY);
		sectionDetailService.saveSectionDetail(sd6);
		
		List<SectionDetail> schedC2 = new ArrayList<>();
		schedC2.add(sd4);
		schedC2.add(sd5);
		schedC2.add(sd6);
		
		//Create SectionDetail of c3
		SectionDetail sd7 = new SectionDetail();
		sd7.setStartTime(LocalTime.of(2, 0));
		sd7.setEndTime(LocalTime.of(3, 00));
		sd7.setRoom(Room.ROOM2);
		sd7.setDay(Day.MONDAY);
		sectionDetailService.saveSectionDetail(sd7);
		
		SectionDetail sd8 = new SectionDetail();
		sd8.setStartTime(LocalTime.of(3,00));
		sd8.setEndTime(LocalTime.of(4,00));
		sd8.setRoom(Room.ROOM3);
		sd8.setDay(Day.WED);
		sectionDetailService.saveSectionDetail(sd8);
		
		SectionDetail sd9 = new SectionDetail();
		sd9.setStartTime(LocalTime.of(5,00));
		sd9.setEndTime(LocalTime.of(6,00));
		sd9.setRoom(Room.ROOM3);
		sd9.setDay(Day.FRIDAY);
		sectionDetailService.saveSectionDetail(sd9);
		
		List<SectionDetail> schedC3 = new ArrayList<>();
		schedC3.add(sd7);
		schedC3.add(sd8);
		schedC3.add(sd9);
		
		
		//Create Section
		Section sec1 = new Section();
		sec1.setCapacity(2);
		sec1.setCourse(c1);
		sec1.setSectionName("e05");
		sec1.setDays(schedC1);
		sec1.setTerm(Term.FIRST);
		sectionService.saveSection(sec1);
		
		//Create Section
		
		Section sec2 = new Section();
		sec2.setCapacity(30);
		sec2.setCourse(c2);
		sec2.setSectionName("e21");
		sec2.setDays(schedC2);
		sec2.setTerm(Term.FIRST);
		sectionService.saveSection(sec2);
		
		Section sec3 = new Section();
		sec3.setCapacity(20);
		sec3.setCourse(c3);
		sec3.setSectionName("e22");
		sec3.setDays(schedC3);
		sec3.setTerm(Term.FIRST);
		sectionService.saveSection(sec3);
		
		
		
		
		//Create Degrees
		Degree d1 = new Degree();
		d1.setDegreeName("Computer Science");
		d1.setRequiredCourses(courses);
		degreeService.saveDegree(d1);
		
		Degree d2 = new Degree();
		d2.setDegreeName("Computer Engineering");
		courses.remove(2);
		d2.setRequiredCourses(courses);
		
		//Save degree
		
		degreeService.saveDegree(d2);
		
		
		
		Student s1 = new Student();
		
		List<Role> studentRoles = new ArrayList<>();
		studentRoles.add(studentRole);
		
		s1.setName("Red");
		s1.setUsername("reduser");
		s1.setPassword(passwordEncoder.encode("redpass"));
		s1.setContactNumber("780879555");
		s1.setEmailAddress("thisEmailCool@yahoo.com");
		s1.setRoles(studentRoles);
		studentService.saveStudent(s1);
		
		Student s2 = new Student();
		
		studentRoles.add(studentRole);
		
		s2.setName("john");
		s2.setUsername("johnuser");
		s2.setPassword(passwordEncoder.encode("johnpass"));
		s2.setContactNumber("11111");
		s2.setEmailAddress("hipster@msn.com");
		s2.setRoles(studentRoles);
		studentService.saveStudent(s2);
		
		
		
		
		
		
	
	}

}
