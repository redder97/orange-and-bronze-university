package com.onbuniversity.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class NoSlotsException extends RuntimeException{
	
	public NoSlotsException(String message) {
		super(message);
	}
	
}
