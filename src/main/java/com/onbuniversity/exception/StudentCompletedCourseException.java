package com.onbuniversity.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class StudentCompletedCourseException extends RuntimeException {
	
	public StudentCompletedCourseException(String message) {
		super(message);
	}
}
