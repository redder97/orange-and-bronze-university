package com.onbuniversity.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IllegalDegreeReferenceException extends RuntimeException{
	
	public IllegalDegreeReferenceException(String message) {
		super(message);
	}
	
}
