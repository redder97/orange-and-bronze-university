package com.onbuniversity.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class StudentAlreadyEnlistedException extends RuntimeException {
	
	public StudentAlreadyEnlistedException(String message) {
		super(message);
	}

}
