package com.onbuniversity.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.onbuniversity.payload.ErrorResponse;

@ControllerAdvice
public class EnlistmentControllerAdvice {
	
	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler({ConflictingScheduleException.class, NoSlotsException.class, StudentAlreadyEnlistedException.class})
	public ResponseEntity<ErrorResponse> handleConflict(RuntimeException e) {
		return error(HttpStatus.CONFLICT, e);
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({IllegalDegreeReferenceException.class, IllegalArgumentException.class})
	public ResponseEntity<ErrorResponse> handleBadRequest(RuntimeException e){
		return error(HttpStatus.BAD_REQUEST, e);
	}
	
	private ResponseEntity<ErrorResponse> error(HttpStatus status, Exception e) {
        return ResponseEntity.status(status).body(new ErrorResponse(e.getMessage(), status.value()));
    }
}
