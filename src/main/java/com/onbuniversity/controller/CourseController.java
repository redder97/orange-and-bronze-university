package com.onbuniversity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.onbuniversity.domain.Course;
import com.onbuniversity.service.CourseService;

@RestController
public class CourseController {
	
	@Autowired
	CourseService courseService;
	
	@GetMapping(value="/secure/admin/courses", produces="application/json")
	public List<Course> getAllCourses(){
		return courseService.getAllCourses();
	}
	
	@PostMapping(value="/secure/save/course", produces="application/json")
	public Course saveCourse(@RequestBody Course course) {
		return courseService.saveCourse(course);
	}
}
