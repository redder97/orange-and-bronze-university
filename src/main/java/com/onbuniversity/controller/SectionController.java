package com.onbuniversity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onbuniversity.exception.ConflictingScheduleException;
import com.onbuniversity.exception.NoSlotsException;
import com.onbuniversity.exception.StudentAlreadyEnlistedException;
import com.onbuniversity.service.CourseService;
import com.onbuniversity.service.SectionService;
import com.onbuniversity.service.StudentService;

@CrossOrigin(origins="*")
@RestController
public class SectionController {
	
	@Autowired
	SectionService sectionService;
	
	@Autowired
	StudentService studentService;
	
	@Autowired
	CourseService courseService;
	
	@GetMapping(value="/secure/admin/sections", produces="application/json")
	public ResponseEntity<?> getAllSections(){
		return ResponseEntity.ok(sectionService.getAllSections());
		
	}
	
	@GetMapping(value="/secure/admin/section/{id}", produces="application/json")
	public ResponseEntity<?> findSection(@PathVariable("id") Long id){
		return ResponseEntity.ok(sectionService.findSectionById(id));
	}
	
	@PostMapping(value="/secure/section/{section_id}/add/{student_id}", produces="application/json")
	public ResponseEntity<?> addStudentToSection(@PathVariable("student_id") Long studentId, 
			@PathVariable("section_id") Long sectionId) {
		
		return ResponseEntity.ok(sectionService.addStudentToSection(studentId, sectionId));
		
	}
	
	@PostMapping(value="/secure/section/{section_id}/remove/{student_id}", produces="application/json")
	public ResponseEntity<?> removeStudentFromSection(@PathVariable("student_id") Long studentId, 
			@PathVariable("section_id") Long sectionId) throws Exception {
		return ResponseEntity.ok(sectionService.removeStudentFromSection(studentId, sectionId));
	}
	


}
