package com.onbuniversity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onbuniversity.service.DegreeService;
import com.onbuniversity.service.StudentService;

/*
 * Define content Type that is accepted. 
 * */
@CrossOrigin(allowedHeaders="*", origins="*", value="*")
@RestController
public class DegreeController {
	
	@Autowired
	DegreeService degreeService;
	
	@Autowired
	StudentService studentService;
	
	@GetMapping(value="/secure/admin/degrees", produces="application/json")
	public ResponseEntity<?> getAllDegrees(){
		return ResponseEntity.ok(degreeService.getAll());
	}
	
	@GetMapping(value="/secure/degree/{id}", produces="application/json")
	public ResponseEntity<?> getDegree(@PathVariable("id") Long id) {
		return ResponseEntity.ok(degreeService.findById(id));
	}
	
	@PostMapping(value="/secure/degree/{degree_id}/student/{student_id}", produces="application/json")
	public ResponseEntity<?> enlistStudentInDegree(@PathVariable("degree_id") Long degreeId, 
			@PathVariable("student_id") Long studentId) {
		return ResponseEntity.ok(degreeService.enlistStudent(degreeId, studentId));
	}
	
	@PostMapping(value="/secure/admin/degree/{id}/students", produces="application/json")
	public ResponseEntity<?> getStudentsByDegree(@PathVariable("id") Long id){
		return ResponseEntity.ok(degreeService.getStudentsByDegree(id));
	}
	
	@PostMapping(value="/secure/degree/{degree_id}/remove/{student_id}", produces="application/json")
	public ResponseEntity<?> removeStudentFromDegree(@PathVariable("degree_id") Long degreeId, 
			@PathVariable("student_id") Long studentId){
		return ResponseEntity.ok(degreeService.removeStudentFromDegree(degreeId, studentId));
	}
	
		
	
}
