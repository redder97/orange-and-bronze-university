package com.onbuniversity.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onbuniversity.dto.NewStudentDTO;
import com.onbuniversity.dto.StudentDTO;
import com.onbuniversity.payload.JwtAuthenticationResponse;
import com.onbuniversity.payload.LoginRequest;
import com.onbuniversity.security.TokenProvider;
import com.onbuniversity.service.DegreeService;
import com.onbuniversity.service.SectionService;
import com.onbuniversity.service.StudentService;
import com.onbuniversity.util.RequestUtil;

@CrossOrigin(origins="*",allowedHeaders="*")
@RestController
public class Controller {
	
	@Autowired
	StudentService studentService;
	
	@Autowired
	SectionService sectionService;
	
	@Autowired
	DegreeService degreeService;
	
	@Autowired
	TokenProvider tokenProvider;
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	private static final Logger logger = LoggerFactory.getLogger(Controller.class);
	
	@PostMapping(value="/signin", consumes="application/json")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, HttpServletRequest request) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        
        Collection<? extends GrantedAuthority> auths = 
        		SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, auths));
    }
	
	@GetMapping(value="/secure/profile", produces="application/json")
	public ResponseEntity<?> setStudentProfile(HttpServletRequest request){
		
			
		String jwt = RequestUtil.getJwtFromRequest(request);
		logger.info("JWT: "  + jwt);
		
		StudentDTO s = studentService.getStudentProfile(tokenProvider.getUsernameFromToken(jwt));
			
		logger.info("student: " + s);
		return ResponseEntity.ok(s);
		
	}
	
	@GetMapping(value="/secure/admin/students", produces="application/json")
	public ResponseEntity<?> getAllStudents(){
		return ResponseEntity.ok(studentService.getAllStudents());
	}
	
	@RequestMapping(value="/create/student", consumes="application/json", produces="application/json")
	public ResponseEntity<?> createStudent(@RequestBody NewStudentDTO student) {
		return ResponseEntity.ok(studentService.createStudentProfile(student));
	}
	
	@RequestMapping(value="/secure/update/student/{id}", produces="application/json")
	public ResponseEntity<?> updateStudent(@PathVariable("id") Long id,
			@RequestBody StudentDTO student){

		return ResponseEntity.ok(studentService.updateStudent(student, id));
	}
	
//	@PostMapping("/secure/admin/delete/student/{id}")
//	public List<Student> deleteStudent(@PathVariable("id") Long id){
//		
//		Student student = studentService.findById(id);
//		
//		List<Section> sections = sectionService.getAllSections();
//		
//		for(Section s : sections) {
//			s.getStudents().removeIf(stud -> stud.getId() == id);
//			sectionService.saveSection(s);
//		}
//		
//		Degree degree = degreeService.findByStudent(student);
//		if(degree != null) {
//			List<Student> students = studentService.findByDegree(degree);
//			students.removeIf(s -> s.getId() == id);
//			degree.setStudents(students);
//			degreeService.saveDegree(degree);
//		}
//		
//		studentService.deleteStudentById(id);
//		return studentService.getAllStudents();
//		
//	}
//	
	
	

	
}
