package com.onbuniversity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onbuniversity.domain.Degree;
import com.onbuniversity.domain.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{
	
	Student findByName(String name);
	Student findByUsername(String username);
	List<Student> findByDegree(Degree degree);

}
