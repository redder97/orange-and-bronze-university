package com.onbuniversity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onbuniversity.domain.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

}
