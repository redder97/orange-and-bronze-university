package com.onbuniversity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onbuniversity.domain.SectionDetail;

@Repository
public interface SectionDetailRepository extends JpaRepository<SectionDetail, Long>{

}
