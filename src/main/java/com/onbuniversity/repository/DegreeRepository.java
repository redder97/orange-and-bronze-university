package com.onbuniversity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onbuniversity.domain.Degree;
import com.onbuniversity.domain.Student;

@Repository
public interface DegreeRepository extends JpaRepository<Degree, Long>{
	
	Degree findByDegreeName(String degreeName);
	Degree findByStudents(Student student);
}
