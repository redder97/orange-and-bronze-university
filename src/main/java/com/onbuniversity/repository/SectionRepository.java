package com.onbuniversity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onbuniversity.domain.Section;
import com.onbuniversity.domain.Student;

@Repository
public interface SectionRepository extends JpaRepository<Section, Long> {
	List<Section> findByStudents(Student student);
	
}
