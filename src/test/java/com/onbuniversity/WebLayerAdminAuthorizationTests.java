package com.onbuniversity;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.onbuniversity.controller.Controller;
import com.onbuniversity.security.TokenProvider;
import com.onbuniversity.service.StudentService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WebLayerAdminAuthorizationTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@InjectMocks
	private Controller controller;
	
	@Autowired
	StudentService studentService;
	
	@Autowired
	TokenProvider tokenProvider;
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	private static String jwt;
	
	/*
	 * 
	 * Set up Authourization level admin
	 * 
	 * */
	@Before
	public void setUpAsAdmin() {
		Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        "admin",
                        "adminPass"
                )
        );
		SecurityContextHolder.getContext().setAuthentication(authentication);
		jwt = tokenProvider.generateToken(authentication);
		
	}
	
	/*
	 * Test that authorization level admin can access admin resources with JWT 
	 * from admin credentials
	 * */
	@Test
	public void tryAdminRestrictedResources() throws Exception {
		HttpHeaders adminAuthHeader = new HttpHeaders();
		adminAuthHeader.set("Authorization", "Bearer " + jwt);
		
		mockMvc.perform(get("/secure/admin/students").headers(adminAuthHeader))
			.andExpect(status().isOk());
		
		mockMvc.perform(get("/secure/admin/degrees").headers(adminAuthHeader))
		.andExpect(status().isOk());
		
		mockMvc.perform(get("/secure/admin/degrees").headers(adminAuthHeader))
		.andExpect(status().isOk());
		
		mockMvc.perform(get("/secure/admin/degrees").headers(adminAuthHeader))
		.andExpect(status().isOk());
	}
	
	

}
