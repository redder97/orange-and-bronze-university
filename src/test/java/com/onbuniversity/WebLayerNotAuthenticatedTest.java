package com.onbuniversity;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WebLayerNotAuthenticatedTest {
	
	@Autowired
	MockMvc mockMvc;
	
	/*
	 * Attempt to access admin restricted resources without authentication
	 * */
	@Test
	public void testAccessToAdminResources() throws Exception {
		mockMvc.perform(get("/secure/admin/students"))
		.andExpect(status().isForbidden());
		
		mockMvc.perform(get("/secure/admin/degrees"))
		.andExpect(status().isForbidden());
		
		mockMvc.perform(get("/secure/admin/sections"))
		.andExpect(status().isForbidden());
		
		mockMvc.perform(get("/secure/admin/courses"))
		.andExpect(status().isForbidden());
	}
	
	/*
	 *Attempt to sign in without providing credentials 
	 * */
	public void testLoginAttemptWithoutCreds() throws Exception {
		mockMvc.perform(post("/signin"))
		.andExpect(status().isBadRequest());
	}
	
	/*
	 * Attempt to access student profile without a
	 * login JSON object
	 * */
	@Test
	public void testAccessToStudentResources() throws Exception {
		mockMvc.perform(get("/secure/profile"))
		.andExpect(status().isBadRequest());
		
	}
	
	
}
