package com.onbuniversity;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.onbuniversity.security.TokenProvider;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WebLayerStudentTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	TokenProvider tokenProvider;
	
	private static String jwt;
	
	
	/*
	 * Set up student level authorization with JWT
	 * 
	 * */
	@Before
	public void setUpUserToken() throws Exception {
		
		Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        "reduser",
                        "redpass"
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
		jwt = tokenProvider.generateToken(authentication);
		
	}
	
	
	/*
	 * 
	 * Ensure token granted to student credentials cannot access admin resources
	 * 
	 * ADMIN Resources denoted by ../admin/.. in api url 
	 * */
	@Test
	public void tryAccessAdminResources() throws Exception {
		HttpHeaders studentAuthHeader = new HttpHeaders();
		studentAuthHeader.set("Authorization", "Bearer " + jwt);
		
		mockMvc.perform(get("/secure/admin/students").headers(studentAuthHeader))
		.andExpect(status().isForbidden());
		
		mockMvc.perform(get("/secure/admin/degrees").headers(studentAuthHeader))
		.andExpect(status().isForbidden());
		
		mockMvc.perform(get("/secure/admin/sections").headers(studentAuthHeader))
		.andExpect(status().isForbidden());
		
		mockMvc.perform(get("/secure/admin/courses").headers(studentAuthHeader))
		.andExpect(status().isForbidden());

	}
	
	/*
	 * Try to access personal profile
	 * 
	 * */
	@Test
	public void tryAccessStudentProfile() throws Exception {
		HttpHeaders studentAuthHeader = new HttpHeaders();
		studentAuthHeader.set("Authorization", "Bearer " + jwt);
		
		mockMvc.perform(get("/secure/profile").headers(studentAuthHeader))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(content().json(""
				+ "{"
				+ "'id': 2,"
				+ "'name':'Red'"
				+ "}"));
		
	}
	
	/*
	 * Test Student's enrollment in degree  
	 * 
	 * */
	@Test
	public void tryEnrollInDegree() throws Exception {
		HttpHeaders studentAuthHeader = new HttpHeaders();
		studentAuthHeader.set("Authorization", "Bearer " + jwt);
		
		mockMvc.perform(post("/secure/degree/1/student/1").headers(studentAuthHeader))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(content().json(""
				+ "{"
				+ "'id': 1,"
				+ "'degree' : 'Computer Science' "
				+ "}"));	
	}
	
	@Test 
	public void tryEnrollInSection() throws Exception {
		HttpHeaders studentAuthHeader = new HttpHeaders();
		studentAuthHeader.set("Authorization", "Bearer " + jwt);
		
		mockMvc.perform(post("/secure/section/1/add/1").headers(studentAuthHeader))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(content().json(""
				+ "{"
				+ "'id': 1, "
				+ "'students': [{'id' : 1}]"
				+ "}"));
		
	}
	

}
